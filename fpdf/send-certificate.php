<?php
	require('fpdf.php');
	require_once('../PHPMailer/class.phpmailer.php');
	
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	
	$data = array();
	
	if (empty($name)){
		$data['success'] = false;
		$data['error'] = "No name";
	}else if (empty($email)){
		$data['success'] = false;
		$data['error'] = "No email";
	}else{
		$pdf = new FPDF();
		$pdf->AddPage('p', 'a4');
		$pdf->SetFont('Arial','B',24);
		$now = date('d F Y');
		$pdf->Cell(180,10,"Congratulations $name - $now",0,1,'C');
		$pdf->Image("certificate.png");
		$doc = $pdf->Output('', 'S');
		
		$mail = new PHPMailer(); // defaults to using php "mail()"

		$body = file_get_contents('email.html');
		//$body             = eregi_replace("[\]",'',$body);

		$mail->AddReplyTo("no-reply@tiow.org","Wellbeing");
		$mail->SetFrom('no-reply@tiow.org', 'Institute of Wellbeing');
		$mail->AddAddress($email, $name);
		$mail->Subject = "Your Wellbeing certificate";

		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; 
		$mail->MsgHTML($body);

		$mail->AddStringAttachment($doc, 'wellbeing-certificate.pdf', 'base64', 'application/pdf');
		if(!$mail->Send()) {
			$data['success'] = false;
			$data['error'] = "Mailer Error: " . $mail->ErrorInfo;
		} else {
			$data['success'] = true;
			$data['message'] = "Message sent! $now";
		}
	}
	
	$json = json_encode($data);
	echo $json;
?>