<?
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	require_once('connect.php');
	
	$password = (empty($_REQUEST['password'])) ? '' : $_REQUEST['password'];
	$first = (empty($_REQUEST['first'])) ? 0 : $_REQUEST['first'];
	$wp = (empty($_REQUEST['wp'])) ? false : true;
	$legal = ($password=='e-!earn1ng');
	$get_registered = ($wp) ? '!' : ''; 
	$sqlerror = '';
	
	if ($legal){
		$sql = "SELECT COUNT(id) FROM users WHERE wpId$get_registered=0";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_row($result);
		$count = $row[0];
		$total = 10;
		$sql = "SELECT id, firstname, lastname, email, city, full FROM users WHERE wpId$get_registered=0 LIMIT $first, $total";
		$result = mysqli_query($conn, $sql);
		$success = false;
		if (!$result){
			$sqlerror = 'SQL Error:'.mysqli_error($conn).' '.$sql;
		}else{
			$users = array();
			while($row=mysqli_fetch_assoc($result)){
				$id = $row['id'];
				$sql = "SELECT id FROM userdata WHERE userId=$id AND type='end'";
				$resultb = mysqli_query($conn, $sql);
				if ($resultb){
					$row['completed'] = (mysqli_num_rows($resultb)==0) ? 'no' : 'yes';
				}else{
					$row['completed'] = 'no';
				}
				$users[] = $row;
			}
			$success = true;
		}
	}
	
	mysqli_close($conn);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>The Institute of Wellbeing: E-Learning Course Dashboard</title>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/jquery.form.js"></script> 
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="js/dashboard.js" type="text/javascript"></script>
<script type="text/javascript">
var first = <? echo $first; ?>;
</script>
</head>

<body>
<? if ($legal){ ?>
Show registered users<input id="regUsers" type="checkbox" <? if ($wp) echo 'checked'; ?>>
<? echo $sqlerror.'<br>Total:'.$count.'<br>'; ?> 
<h3>Users</h3>
<table border="1">
<tr><th>id</th><th>First name</th><th>Last name</th><th>Email</th><th>City</th><th>Full</th><th>Completed</th><th>&nbsp;</th></tr>
<?
	if ($success){
		foreach($users as $user){
			echo '<tr><td>'.$user['id'].'</td>';
			echo '<td>'.$user['firstname'].'</td>';
			echo '<td>'.$user['lastname'].'</td>';
			echo '<td>'.$user['email'].'</td>';
			echo '<td>'.$user['city'].'</td>';
			$full = ($user['full']==0) ? '<div id="full'.$user['id'].'"><a href="javascript:makeFull('.$user['id'].')"><input type="button" value="upgrade"></a></div>' : 'yes';
			echo '<td>'.$full.'</td>';
			echo '<td>'.$user['completed'].'</td>';
			echo '<td><a href="javascript:selectUser('.$user['id'].')"><input type="button" value="Get details"></a></td></tr>';
		}
	}
?>
</table>
<? 
	if ($success){
		if ($count>$total){
			$pages = ceil($count/$total);
			$page = ceil($first/$total);
			$min = $page-5;
			$max = $page+5;
			if ($min<0) $min=0;
			while (($max*$total)>$count) $max--;
			$wpStr = ($wp) ? '&wp=1' : '';
			$path = 'dashboard.php?password='.$password.$wpStr.'&first=';
			if ($max>$min){
				if ($page>0){
					echo '<a href="'.$path.'0">&lt;&lt;</a>&nbsp;&nbsp;';
					echo '<a href="'.$path.(($page - 1)*$total).'">&lt;</a>&nbsp;&nbsp;';
				}
				for($i=$min; $i<$max; $i++){
					if ($i==$page){
						echo $i.'&nbsp;&nbsp;';
					}else{
						echo '<a href="'.$path.($i * $total).'">'.$i.'</a>&nbsp;&nbsp;';
					}
				}
				if (($page*$total)<($count-$total)){
					echo '<a href="'.$path.(($page+1)*$total).'">&gt;</a>&nbsp;&nbsp;';
					echo '<a href="'.$path.($count - $total).'">&gt;&gt;</a>&nbsp;&nbsp;';
				}
			}
		}
	}
	if (!$success) echo $sqlerror;
?>
	<div id="user"></div>
<? }else{ ?>
This page is only for the administrator.
<? } ?>
</body>
</html>