<?
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);

	require_once('connect.php');
	
	$loginInfo = new stdClass; 
	$loginInfo->loggedIn = false;
	
	if (empty($_REQUEST['user_id'])){
		$loginInfo->errorCode = 1;
		$loginInfo->errorMessage = 'No user id';
	}else if (empty($_REQUEST['password'])){
		$loginInfo->errorCode = 2;
		$loginInfo->errorMessage = 'No password';
	}else if ($_REQUEST['password'] != 'e-!earn1ng'){
		$loginInfo->errorCode = 3;
		$loginInfo->errorMessage = 'Wrong password';
	}else{
		$userId = $_REQUEST['user_id'];
		$sql = "SELECT * FROM users WHERE wpId=$userId";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			$loginInfo->errorCode = 4;
			$loginInfo->errorMessage = 'SQL Error:'.$sql.' '.mysqli_error($result);
		}else if (mysqli_num_rows($result)==0){
			$loginInfo->loggedIn = true;
			$loginInfo->wpId = $_REQUEST['user_id'];
			$loginInfo->email = $_REQUEST['email'];
			$loginInfo->registered = false;
		}else{
			$row = mysqli_fetch_assoc($result);
			$id = $row['id'];
			$sql = "SELECT id FROM completed WHERE DATE_ADD(completeDate, INTERVAL 3 MONTH) > NOW() AND userId=$id";
			$result = mysqli_query($conn, $sql);
			if ($result && mysqli_num_rows($result)>0){
				header('Location: index_refresher.php?user_id='.$_REQUEST['user_id'].'&password=e-!earn1ng&email='.$_REQUEST['email']);
			}else{
				$loginInfo->debugMessage = $sql;
			}
			$full = $row['full'];
			if ($full==0){
				$elapsedTime = time() - strtotime($row['registerTime']);
				$threedays = 60 * 60 * 24 * 3;
				if ($elapsedTime>$threedays){
					$loginInfo->errorCode = 5;
					$loginInfo->errorMessage = 'Trial period expired';
				}else{
					$full = 1;
				}
			}
			if ($full==1){
				$loginInfo->loggedIn = true;
				$loginInfo->registered = true;
				$loginInfo->id = $row['id'];
				$loginInfo->email = $row['email'];
				$loginInfo->userName = $row['firstname'].' '.$row['lastname'];
				session_start();
				$_SESSION['userId'] = $row['id'];
			}
		}
		//echo 'UserID:'.$_REQUEST['user_id'].'<br>';
		//echo 'User Email:'.$_REQUEST['email'].'<br>';
	}
	
	//print_r($loginInfo);
	
	mysqli_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Institute of Wellbeing e-learning resource</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/jquery.form.js"></script> 
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="js/wellbeing.js" type="text/javascript"></script>
<script language="javascript">
<? 
	if ($loginInfo->loggedIn){
		echo "   var loginInfo = { loggedIn:true };\n";
		if ($loginInfo->registered){
			echo "   loginInfo.registered = true;\n";
			echo "   loginInfo.id = ".$loginInfo->id.";\n";
			echo "   loginInfo.email = \"".$loginInfo->email."\";\n";
			echo "   loginInfo.userName = \"".$loginInfo->userName."\";\n";
			if (!empty($loginInfo->debugMessage)) echo "   loginInfo.debugMessage = \"".$loginInfo->debugMessage."\";\n";
		}else{
			echo "   loginInfo.registered = false;\n";
		}
	}else{
		echo "   loginInfo.loggedIn = false;\n";
	}
?>
$(window).on('message', function(e) {
	var tmp = (eval('(' +e.originalEvent.data+')'));
	window.hasOwnProperty = window.hasOwnProperty || Object.prototype.hasOwnProperty;
	if(tmp.hasOwnProperty('nhs_redirect')){
		window.location.href = tmp.nhs_redirect;
	}
});    
</script>
</head>

<body>
<? if ($loginInfo->loggedIn){ ?>
<div id="outer">
    <div id="container">  
        <div id="learning_panel">   
            <div class="bg">
            	<div class="text">LEARNING AIM<br/><br/>To understand how to help make your setting a place of inclusion and wellbeing and what you and your colleagues can do to make it welcoming for families</div>     
            </div>
        </div> 
        <div id="your_turn_panel">   
            <div class="pic"><img src="images/hand.png" /></div>
            <div class="heading">YOUR<br/>TURN</div>
                <div class="text">Spend a few moments thinking how you would define wellbeing. These definitions (to come) might help. Write down your own definition of wellbeing in your online diary.<br/><br/>Complete this questionnaire to find a measure of your own wellbeing.<br/><br/>You will get your results in the next module.<br/><br/></div>
            <div id="tip_panel">
                <div class="text">LEARNING POINT<br/>Learning point goes here. There isn't always a learning point.</div>
            </div>     
        </div> 
<div class="pagecontent">
        	<div class="subtitle"></div>
        	<div class="content_main"></div>
      <div id="content-nav"> 
       	  <div class="back-btn"><a href="javascript:selectSubPage(false);">Back</a></div>
          <div class="next-btn"><a href="javascript:selectSubPage(true);">Next</a></div>
</div> 
        </div>
        <div class="img-right"><img src="images/3girls.jpg" /></div>
        <div class="img-left"><img src="images/3girls.jpg" /></div>
        <!--</div> -->
        <div id="intro">
        	<div class="title">Increasing Wellbeing & Inclusion in Early Years Settings – e-Learning Resource</div>
		  <div class="content_main"><p>Welcome to the Institute of Wellbeing's e-learning resource on increasing inclusion and wellbeing in early years settings.<br/><br/> 
We believe that understanding the role of wellbeing in people's lives, and teaching them how to achieve it, is key to helping them make progress and enjoy their lives more fully.<br/><br/>
As an early years practitioner you are in a position to help improve the wellbeing of children and their families at a crucial point in children's development, when the foundations of their future lives are being laid. Children from families with high levels of wellbeing make faster and better progress in their education.<br/><br/>
Working with all families in an inclusive way is a requirement of the Early Years Foundation framework. Ofsted will also look for evidence of a partnership with parents and that all children and families, especially those with special educational needs and disabilities, are equally able to access good early years settings.</p>					
			</div>
            
    	</div>
        <div id="wsas"> 
   		<div class="title">The Work and Social Adustment Scale (WSAS)</div>
		  <div class="text">To rate your problems look at each section and determine on the scale provided how much your problems impair your ability to carry out activities in important areas of your life. </div>
          <div class="progress">Question 1 of 5</div> 
  		  <div class="slider1">
    <div class="slider_bg"></div>
        		<div class="slider_title">How much do your problems hinder you in the area of WORK 
- if you are retired or choose not to have a job for reasons unrelated to your problem, please tick N/A (not applicable)</div>
        		<div id="wsas-slider" class="slider"></div> 
        		<div class="slider_label1">not at all</div> 
          		<div class="slider_label2">very severely</div>
          		<div class="slider_scale">0 1 2 3 4 5 6 7 8 9 </div>
                <div class="slider_scale10">10</div>  
  		  </div>
            <div class="feedback">Thank you for taking part. Your Wellbeing score is xxxx</div>  
            <div class="back-btn"><a href="javascript:nextWSAS(false);">Back</a></div>
            <div class="skip-btn"><a href="javascript:skipWSAS();">Skip</a></div>
            <div class="next-btn"><a href="javascript:nextWSAS(true);">Next</a></div>
	  </div><!--End wsas --> 
        <div id="intro_panel">  
    		<div class="bg"> 
        		<div class="close"><a href="javascript:closeIntro()" title="Close Intro Panel"><img src="images/close.png" /></a></div>
        		<div class="pic"><img src="" alt=""/></div>
       		  <div class="text"></div>          
        	</div>
        	<div class="pointer"><img src="images/triangle.png" /></div>
    	</div> 
        <div id="drag_activity"> 
        	<div class="title">This is the title</div>
       	  <div class="maintext"><p>Your setting should be a place where both children and their parents feel equally comfortable, no matter where they come from, or what they may have experienced in their lives.</p><p>Can you answer the following questions with a yes? The more questions you can answer positively, the more likely your setting is to already be a place of wellbeing. Where you answer no, you will find suggestions for improving the setting. Drag the question into the correct box.</p></div>
          <div class="yes_box"></div>
          <div class="no_box"></div>
          <div class="drag_box">
          		<div class="drag_question">?</div>
            	<div class="drag_text">This will be dragged</div>
          </div> 
            <div class="popup">
            	<div class="bg">
        			<div class="close"><a href="javascript:closeDragPopup()" title="Close Popup Panel"><img src="images/close.png" /></a></div>
        			<div class="text">This is the text</div>          
                </div>
          </div>
		</div>
		<div id="nav-bar"> 
            <ul>  
                <li><a class="tab1" href="javascript:selectModule(1)" title="Module 1">Module 1</a></li>  
                <li><a class="tab2" href="javascript:selectModule(2)" title="Module 2">Module 2</a></li>  
                <li><a class="tab3" href="javascript:selectModule(3)" title="Module 3">Module 3</a></li>  
                <li><a class="tab4" href="javascript:selectModule(4)" title="Module 4">Module 4</a></li>  
                <li><a class="tab5" href="javascript:selectModule(5)" title="Module 5">Module 5</a></li>  
                <li><a class="tab6" href="javascript:selectModule(6)" title="Module 6">Module 6</a></li>
                <li><a class="tab7" href="javascript:selectModule(7)" title="Module 7">Module 7</a></li>  
                <li><a class="tab8" href="javascript:selectModule(8)" title="Module 8">Module 8</a></li>  
            </ul>  
        </div>  
        
        <div class="header">
        	<img src="images/logo.png" width="168" height="65" style="display:none;"/>
            <div class="nav_btns">
   	      		<a href="javascript:rewindPressed();" title="Rewind"><img class="nav_btn" src="images/rewind_btn.png" width="45" height="37" alt="Rewind" /></a>
                <a href="javascript:backPressed();" title="Back"><img class="nav_btn" src="images/back_btn.png" width="24" height="37" alt="Back" /></a>
                <a href="javascript:nextPressed();" title="Next"><img class="nav_btn" src="images/next_btn.png" width="24" height="37" alt="Next" /></a>
            </div>
        	<div class="login"></div>
        </div>
        <div id="img_diary"><a href="javascript:showDiary()" title="Show Diary"><img src="images/diary_front_small.png" width="129" height="150" /></a></div> 
        <div id="pairs">
        	<div class="pairs_table">
        		<div class="pairs_row">
            		<div class="holder"><a href="javascript:pairsPressed(0,0)" title="Pair"><div id="pair0_0" class="pair_btn">This is the copy</div></a></div>
                	<div class="holder"><a href="javascript:pairsPressed(0,1)" title="Pair"><div id="pair0_1" class="pair_btn">This is the copy</div></a></div>
                	<div id="pair0_tickcross" class="tickcross"></div>
            	</div>
                <div class="pairs_row">
            		<div class="holder"><a href="javascript:pairsPressed(1,0)" title="Pair"><div id="pair1_0" class="pair_btn">This is the copy</div></a></div>
                	<div class="holder"><a href="javascript:pairsPressed(1,1)" title="Pair"><div id="pair1_1" class="pair_btn">This is the copy</div></a></div>
                	<div id="pair1_tickcross" class="tickcross"></div>
            	</div> 
                <div class="pairs_row">
            		<div class="holder"><a href="javascript:pairsPressed(2,0)" title="Pair"><div id="pair2_0" class="pair_btn">This is the copy</div></a></div>
                	<div class="holder"><a href="javascript:pairsPressed(2,1)" title="Pair"><div id="pair2_1" class="pair_btn">This is the copy</div></a></div>
                	<div id="pair2_tickcross" class="tickcross"></div>
            	</div> 
                <div class="pairs_row">
            		<div class="holder"><a href="javascript:pairsPressed(3,0)" title="Pair"><div id="pair3_0" class="pair_btn">This is the copy</div></a></div>
                	<div class="holder"><a href="javascript:pairsPressed(3,1)" title="Pair"><div id="pair3_1" class="pair_btn">This is the copy</div></a></div>
                	<div id="pair3_tickcross" class="tickcross"></div> 
            	</div> 
                <div class="pairs_row">
            		<div class="holder"><a href="javascript:pairsPressed(4,0)" title="Pair"><div id="pair4_0" class="pair_btn">This is the copy</div></a></div>
                	<div class="holder"><a href="javascript:pairsPressed(4,1)" title="Pair"><div id="pair4_1" class="pair_btn">This is the copy</div></a></div>
                	<div id="pair4_tickcross" class="tickcross"></div>
            	</div> 
                <div class="pairs_row">
            		<div class="holder"><a href="javascript:pairsPressed(5,0)" title="Pair"><div id="pair5_0" class="pair_btn">This is the copy</div></a></div>
                	<div class="holder"><a href="javascript:pairsPressed(5,1)" title="Pair"><div id="pair5_1" class="pair_btn">This is the copy</div></a></div>
                	<div id="pair5_tickcross" class="tickcross"></div>
            	</div>  
            </div>
          <a href="javascript:pairsDonePressed()" title="Done"><div class="done_btn">Done</div></a>
        </div>
		<div id="question-buttons">
<div class="buttons">  
				<ul>
                	<li><a href="javascript:showQuestionBoxPopup(1)" title"Show Popup"><img src="images/pointer.png" /></a></li>
                    <li><a href="javascript:showQuestionBoxPopup(2)" title"Show Popup"><img src="images/pointer.png" /></a></li>
                    <li><a href="javascript:showQuestionBoxPopup(3)" title"Show Popup"><img src="images/pointer.png" /></a></li>
                    <li><a href="javascript:showQuestionBoxPopup(4)" title"Show Popup"><img src="images/pointer.png" /></a></li>
                </ul>
          </div>
        	<div class="popup">
            	<div class="bg">
        			<div class="close"><a href="javascript:closeQuestionBoxPopup()" title="Close Popup Panel"><img src="images/close.png" /></a></div>
       			  <div class="text">This is the text</div>          
        			<div class="pointer">&nbsp;</div> 
                </div>
            </div>
        </div>
        
        <div id="nav-bottom" style="display:block; bottom:0px;">  
            <ul>  
                <li><a class="tab1" href="javascript:selectPage(1)" title="Section 1">Module 1</a></li>  
                <li><a class="tab2" href="javascript:selectPage(2)" title="Section 2">Module 2</a></li>  
                <li><a class="tab3" href="javascript:selectPage(3)" title="Section 3">Module 3</a></li>  
                <li><a class="tab4" href="javascript:selectPage(4)" title="Section 4">Module 4</a></li>  
                <li><a class="tab5" href="javascript:selectPage(5)" title="Section 5">Module 5</a></li>  
            </ul>  
        </div>   
        <div id="start_course" onmouseover="startCourseRollover()" onmouseout="startCourseRollout()">
        	<a href="javascript:register()">
        		<img src="images/hand.png" />
        		<div class="text">Start<br />Course</div>
            </a>
        </div>  
        <div id="login">  
        	<div class="title">Login</div>
            <form id="loginForm" action="ws/login.php" method="post" onsubmit="return loginValidate(this);">
            	<div class="error">Error</div> 
				<div class="label1">Email:</div>
            	<div class="input1"><input name="email" type="text" class="text_input"/></div>
            	<div class="label2">Password:</div>
                <div class="input2"><input name="password" type="password" class="text_input" /></div>
                <input type="submit" value="Login" class="submitBtn" />
            </form>
            <div class="forgotten"><a href="javascript:forgottenPressed()" title="Forgotten Password?">Forgotten Password?</a></div>
            <div class="register"><a href="javascript:register()" title="Register">Register</a></div>
            <div class="base"><img src="images/login_base.jpg" /></div>
        </div>  
        <div id="forgotten">  
        	<div class="title">Forgotten Password</div>
            <div class="back"><a href="javascript:showLogin()">Back</a></div>
            <form id="forgottenForm" action="ws/forgotten.php" method="post" onsubmit="return forgottenValidate(this);">
            	<div class="error">Error</div> 
				<div class="label1">Email:</div>
            	<div class="input1"><input name="email" type="text" class="text_input"/></div>
            	<div class="message">Enter your email address then press the Send button</div>
                <input type="submit" value="Send" class="submitBtn" />
            </form>
            <div class="base"><img src="images/login_base.jpg" /></div>
        </div>      
		<div id="register">
        	<div class="title">Register</div> 
            <div class="back"><a href="javascript:registerBack()">Back</a></div>	
            <form id="registerForm" action="ws/register.php" method="post" onsubmit="return registerValidate(this, 5);">
            	<div class="error">Error</div> 
            	<div id="register1" class="list" style="display:block;">
<ul>
						<input type="hidden" name="email" value="<? echo $loginInfo->email; ?>" />
                        <input type="hidden" name="wpId" value="<? echo $loginInfo->wpId; ?>" />
                    	<li><span class="label">First name</span><input type="text" class="text_input" name="firstname" placeholder="required" /></li>
                        <li><span class="label">Last name</span><input type="text" class="text_input" name="lastname" placeholder="" /></li>
                        <li><span class="label">Address 1</span><input type="text" class="text_input" name="address1" placeholder="required" /></li>
                        <li><span class="label">Address 2</span><input type="text" class="text_input" name="address2" placeholder="" /></li>
                        <li><span class="label">City</span><input type="text" class="text_input" name="city" placeholder="required" /></li>
                        <li><span class="label">County</span><input type="text" class="text_input" name="county" placeholder="required" /></li>
                        <li><span class="label">Postcode</span><input type="text" class="text_input" name="postcode" placeholder="required" /></li>
                  </ul>
                </div>
<div id="register2" class="list" style="display:none;">
            		<ul>
                    	<li><span class="heading">You</span></li><br /> 
                        <li><span class="label">Contact Number</span><input type="text" class="text_input" name="your_number" placeholder="required" /></li>
                        <li><span class="label">Date of birth</span><input id="register_dob" type="text" class="text_input" name="your_dob" placeholder="" /></li><br />
                        <li><span class="heading">Supervisor</span></li>
                        <li><span class="label">Name</span><input type="text" class="text_input" name="supervisors_name" placeholder="" /></li>
                        <li><span class="label">Email</span><input type="text" class="text_input" name="supervisors_email" placeholder="" /></li>
                        <li><span class="label">Contact Number</span><input type="text" class="text_input" name="supervisors_number" placeholder="" /></li>
                    </ul>
              </div>
                <div id="register3" class="list" style="display:none;">
            		<ul>
                    	<li><span class="heading">Your Early Years Setting</span></li><br /> 
                        <li><span class="label">Name</span><input type="text" class="text_inputb" name="early_name" placeholder="Name of your setting." /></li>
                        <li><span class="label">Type</span><input type="text" class="text_inputb" name="early_type" placeholder="What type of setting is it?" /></li>
                        <li><span class="label">Services</span><textarea class="text_area" name="early_clients" placeholder="Tell us about the communities/population that uses your services."></textarea></li>
                        <li><span class="label">Practice</span><textarea class="text_area" name="early_practice" placeholder="How does your setting currently deliver inclusive wellbeing practice?"></textarea></li>
                    </ul>
                </div>
                <div id="register4" class="list" style="display:none;">
            		<ul>
                    	<li><span class="heading">About you</span></li>
                        <li><span class="label">Job title</span><input type="text" class="text_inputb" name="you_jobtitle" placeholder="What is your job title?" /></li>
                        <li><span class="label">Team</span><input type="checkbox" class="checkbox" name="you_team" placeholder="Do you manage a team?" /></li>
                        <li><span class="label">Team size</span><input id="team_spinner" class="spinner" name="you_teamsize"  /><span class="team-info">&nbsp;If yes how many are in the team?</span></li>
                        <li><span class="label">Motivation</span><textarea class="text_area" name="you_motivation" placeholder="Please tell us why you are taking this training?"></textarea></li>
                        <li><span class="label">Ambition</span><textarea class="text_area" name="you_ambition" placeholder="What do you hope to learn through this training?"></textarea></li>
                        <li><span class="label">Progress</span><input type="text" class="text_inputb" name="you_progress" placeholder="What areas would you like to improve?" /></li>
                    </ul>
                </div>
                <div id="register5" class="list" style="display:none;">
            		<ul>
                    	<li><span class="heading">Experience</span></li> 
                        <li><span class="label">How did you hear?</span><textarea class="text_area" name="experience_hear" placeholder="How did you hear about this training?"></textarea></li>
                        <li><span class="label">Experience</span><textarea class="text_area" name="experience_experience" placeholder="What is your experience of Inclusion and Wellbeing practice?"></textarea></li>
                        <li><span class="label">Qualifications</span><textarea class="text_area" name="experience_qualifications" placeholder="What Early Years qualifications do you have?"></textarea></li>
                    </ul>
                </div>
                 <div id="continue-btn" class="submitBtn" style="text-align:center;"><a href="javascript:registerNextPage()">Continue</a></div>
                <input type="submit" value="Register" id="submit-btn" class="submitBtn" style="display:none;"/> 
            </form>
            <div class="base"><img src="images/login_base.jpg" /></div>
        </div>
      <div id="diary"> 
        	<img src="images/diary.png" />
        	<div class="tabs"></div>
            <div class="title">Module 1</div>
          <a href="javascript:closeDiary()" title="Close"><div class="close"></div></a>
            <form id="diaryForm" action="ws/diary.php" method="post">
            	<input type="hidden" value="save" />
           	  <div class="text_left"><textarea name="textleft" class="text_area1" placeholder="Enter your text here. Pressing the close button will save this."></textarea></div>
           	  <div class="text_right"><textarea name="textright" class="text_area2"></textarea></div>
          </form>
  </div> 
		<div id="course_start">
    		<div class="title">Getting Started</div>
   			<div class="hello">Hello xxxx</div>
   			<div class="congrats">Congratulations on taking the time to improve your skills as an early years practitioner.<br />This resource is intended to increase your confidence and skills in working with children and families who may be different to you. Before you start take a moment to answer a few questions about your own practice and your setting. We will ask some of these questions at the end of the resource to help you judge how much your understanding has improved through the process.<br /><br />
            <div class="subheading">About Your Practice</div>
</div>
   	  		<div class="slider1">
   				<div class="slider_bg"></div>
       		  <div class="slider_title">How confident are you in your knowledge and understanding of wellbeing?</div>
       		  <div id="start-slider1" class="slider"></div> 
       		  <div class="slider_label1">very unconfident</div> 
       		  <div class="slider_label2">completely confident</div>
       		  <div class="slider_scale">0 1 2 3 4 5 6 7 8 9 </div>
              <div class="slider_scale10">10</div>   
   	  		</div>
            <div class="next-btn"><a href="javascript:questionaireNextPressed();">Next</a></div> 
   	  		<div class="subtitle">About This Resource</div>
   			<div class="text">Each screen tackles a different area and ends with a short task for you to do which will reinforce the message of that screen. Most involve writing in your online diary. You will not be able to move on to the next screen until you have completed the diary entry for the previous one.</div>
         	<div class="base"><img src="images/login_base.jpg" /></div> 
    		<a href="javascript:startModule()" title="Start Module"><div class="start_module"></div></a>
	  </div><!--End course_start -->
      <div id="quiz"> 
<div class="title">Congratulations [Name]!</div>
   			<div class="text"><p>You have now completed the Institute of Wellbeing's Increasing inclusion and wellbeing in early years settings resource. Well done for your hard work. You should now have the tools, knowledge and understanding to support the wellbeing of the families of the children in your care and to make sure all families feel included in your setting.
<br /><br />
Take this simple quiz to find out if you have remembered all that you have learned.</p></div>
	<div class="progress">Question X of X</div>
    <div class="question">This is the question.</div> 
  		<a href="javascript:answerPressed(1);"><div id="answer1" class="answer">  
       	  		<div class="answer_bg"></div> 
            	<div class="answer_title">Answer</div> 
                <div class="answer_tick"></div>
        </div></a>
          <a href="javascript:answerPressed(2);"> <div id="answer2" class="answer">
   	      		<div class="answer_bg"></div> 
            	<div class="answer_title">Answer</div>
                <div class="answer_tick"></div>
            </div></a>
			<a href="javascript:answerPressed(3);"><div id="answer3" class="answer">
				<div class="answer_bg"></div> 
            	<div class="answer_title">Answer</div>
                <div class="answer_tick"></div>
            </div> </a>
            <div class="next-btn"><a href="javascript:quizNextPressed();">Next</a></div> 
		<div class="base"><img src="images/login_base.jpg" /></div> 
		<a href="javascript:endQuiz()" title="End quiz"><div class="complete">Done</div></a>
  		</div><!--End quiz -->  
		<div id="course_end">
    		<div class="title">Congratulations [Name]!</div>
   			<div class="text"><p>You have now completed the Institute of Wellbeing's Increasing inclusion and wellbeing in early years settings resource. Well done for your hard work. You should now have the tools, knowledge and understanding to support the wellbeing of the families of the children in your care and to make sure all families feel included in your setting.</p><p>In three months' time we will send you a short refresher module to reinforce the important messages of improving wellbeing, and to measure how your work has changed as a result of this resource. Good luck in all your future work with children and families.</p><p>Before you print out your certificate, tell us how much your understanding has improved through the process of completing the resource, by marking the following scales:</p></div>
            <div class="subheading">About Your Practice</div>
   	  		<div class="slider1">
   				<div class="slider_bg"></div>
       		  <div class="slider_title">How confident are you in your knowledge and understanding of wellbeing?</div>
       		  <div id="end-slider1" class="slider"></div> 
       		  <div class="slider_label1">very unconfident</div> 
       		  <div class="slider_label2">completely confident</div>
       		  <div class="slider_scale">0 1 2 3 4 5 6 7 8 9 </div> 
              <div class="slider_scale10">10</div>  
   	  		</div>
  			 <div class="next-btn"><a href="javascript:questionaireNextPressed();">Next</a></div>  
			<div class="base"><img src="images/login_base.jpg" /></div> 
			<a href="javascript:downloadCertificate()" title="Download Certificate"><div class="download_certificate">Download Certificate</div></a>
  		</div><!--End course_end -->  
<div id="popup_panel">  
    		<div class="bg">
        		<div class="close"><a href="javascript:closePopup()" title="Close Panel"><img src="images/close.png" /></a></div>
        		<div class="pic"><img src="" alt=""/></div>
       		  <div class="text"></div>          
        	</div>
    	</div>
<div id="questionaire">
       	  <div style="position:absolute; left:0px; top:0px; width:100%; height:100%; background-color:rgba(255,255,255,0.8);"></div>
            <div class="close-btn"><a href="javascript:close_questionaire();" title="Close questionaire"><img src="images/close.png" alt="close" /></a>
        	<div id="assessment_webpart_wrapper" style="position:absolute; left:-365px; top:0px; width:362px;">
            	<iframe style="width:100%;height:435px;" title="self assessments" src="http://media.nhschoices.nhs.uk/tools/documents/self_assessments_js/assessment.html?ASid=43&syndicate=true" frameborder="no" scrolling="no"></iframe>
				<div id="assessment_webpart_branding" style="float:right;"><a href="http://nhs.uk/tools/" alt="content provided by NHS Choices"><img src="http://media.nhschoices.nhs.uk/tools/documents/self_assessments_js/images/syndication.png" width="264" height="38" border="0" alt="content provided by NHS Choices" /></a></div>
        	</div>   
        </div>       
    </div><!--End container -->
</div><!--End outer -->
<? }else{ 
	if ($loginInfo->errorCode==5){  ?>
Your trial period has expired. Please register for the full course to continue using the course.
	<? }else{ ?>
You must login and register for this course at the institute of wellbeing <a href="http://theinstituteofwellbeing.com" target="_parent">website</a>
<? }} ?>
</body>
</html>