// JavaScript Document

$(document).ready(function(){
	if (typeof console == "undefined") {
		this.console = {log: function() {}};
	}
	console.log("Document ready");
	$('#regUsers').change(function() {
        if($(this).is(":checked")) {
			window.location = "dashboard.php?password=e-!earn1ng&wp=true&first=" + first;
        }else{
			window.location = "dashboard.php?password=e-!earn1ng&first=" + first;
		}   
    });
});

function makeFull(id, elm){
	console.log('makeFull:' + id);
	$.post( "ws/dashboard.php?method=make_full&user_id=" + id, function(data){
		console.log("makeFull returned " + data);
		var json = JSON.parse(data);
		if (json.success) $('#full' + id).text('yes');
	});
}
	
function selectUser(id){
	console.log('selectUser:' + id);
	$.post( "ws/dashboard.php?method=get_user&user_id=" + id, function(data){
		console.log("selectUser returned " + data);
		var json = JSON.parse(data);
		if (json.success){
			console.log("selectUser success");
			var elm = $('#user');
			var str = '<H1>' + json.users.firstname + ' ' + json.users.lastname + '</H1>';
			str += '<p><a href="mailto:' + json.users.email + '">' + json.users.email + '</a></p>';
			str += '<p>' + json.users.address1 + '<br>' + json.users.address2 + '<br>' + json.users.city + '<br>' + json.users.county + '<br>' + json.users.postcode + '</p>';
			str += '<p>' + json.users.phone + '</p>';
			str += '<p>Date of birth: ' + json.users.dob + '</p>';
			str += '<h3>Setting</h3>';
			if (json.setting!=null){
				str += 'Name:' + json.setting.name + '<br>';
				str += 'Type:' + json.setting.type + '<br>';
				str += 'Clients:' + json.setting.clients + '<br>';
				str += 'Practice:' + json.setting.practice + '<br>';
			}
			str += '<h3>Experience</h3>';
			if (json.experience!=null){
				str += 'Hear:' + json.experience.hear + '<br>';
				str += 'Experience:' + json.experience.experience + '<br>';
				str += 'Qualifications:' + json.experience.qualifications + '<br>';
			}
			str += '<h3>Me</h3>';
			if (json.you!=null){
				str += 'Job title:' + json.you.jobtitle + '<br>';
				str += 'Team:' + json.you.team + '<br>';
				str += 'Team size:' + json.you.teamsize + '<br>';
				str += 'Motivation:' + json.you.motivation + '<br>';
				str += 'Ambition:' + json.you.ambition + '<br>';
				str += 'Progress:' + json.you.progress + '<br>';
			}
			str += '<h3>Data</h3>';
			if (json.data!=null){
				var startend = {};
				for(var i=0; i<json.data.length; i++){
					switch(json.data[i].type){
						case 'start':
						startend.start = JSON.parse(json.data[i].data);
						break;
						case 'end':
						startend.end = JSON.parse(json.data[i].data);
						break;
						case 'diary':
						str += '<p><h4>Diary</h4></p><ul>';
						var diary = JSON.parse(json.data[i].data);
						for(var j=0; j<diary.length; j++){
							var entry = diary[j];
							if (entry!=null) str += '<li>Module ' + j + '<br>' + entry.textleft + '<br>' + entry.textright + '</li>';
						}
						str += '</ul>';
						break;
					}
				}
				str += '<h4>Pre and post evaluation</h4>';
				if (startend.start != null){
					if (startend.end != null){
						var delta = 0;
						var total = 0;
						str += '<p>';
						for(var i=0; i<startend.start.length; i++){
							str += startend.start[i] + '&gt;' + startend.end[i] + ' ';
							delta += (startend.end[i] - startend.start[i]);
							total += (10 - startend.start[i]);
						}
						str += '</p><p>Wellbeing change:' + Math.round((delta/total) * 100)  + '%</p>';
					}else{
						str += '<p>Course not completed start values are ' + startend.start + '</p>';
					}
				}else{
					str += '<p>User has not yet started the course</p>';
				}
			}
			elm.html(str);
		}else{
			console.log("selectUser error:" + json['error']);
		}
	});
}