// JavaScript Document
var debug = true;
var showQuestionaire = false;
var selected = { module:-1, page:-1, subpage:0 };
var wsas = { page:0, choices:[5,5,5,5,5] };
var quiz = { page:0, results:[] };
var data;
var user;
var email;
var registerPage;
var errorInfo = { elm:null, id:null };
var userdata;
var startCourseCalled = false;
var drag_data;
var interactive_data;
var preloadImages;
var questionaire;

function wbLog(str){
	if (debug) console.log(str);
}

function preload() {
	var urls = new Array("diary_tabs0001.png", "diary_tabs0002.png", "diary_tabs0003.png", "diary_tabs0004.png", "diary_tabs0005.png", "diary_tabs0006.png", "diary_tabs0007.png", "diary_tabs0008.png",
						 "3girls.jpg", "3kids.jpg", "3kids_resized.jpg", "3kids_small.jpg", "black_family.png", "black_family_resized.png", "couple arguing.png",
						 "diary.png", "diary_cover.png", "diary_front.png", "diary_front_small.png", "family.png", "family_upset.png", "girl_sad.png",
						 "hand.png", "kids_with_bricks.png", "kitchen.png" );	
	preloadImages = new Array();
	for (i = 0; i < urls.length; i++) {
		var image = new Image();
		image.src = "images/" + urls[i];
		preloadImages.push(image);
	}
}

function forgottenPressed(){
	wbLog("Forgotten pressed");
	$("#login").hide();
	$("#forgotten").show();
	$("#forgotten div.label1").show();
	$("#forgotten div.input1").show();
	$("#forgotten .submitBtn").show();
	$("#forgotten .submitBtn").text("Send");
	$("#forgotten div.message").text("Enter your email address then press the Send button.");
}

/*function loginToCourse(){
	console.log("loginToCourse "); 
	$.getJSON("ws/login.php?id=" + loginInfo.id, function(data){
		var json = JSON.parse(data);
		if (json.success){
			wbLog("loginForm retured " + datab);
			user = { id:json.userId, name:json.userName };
			loggedIn();
			if (startCourseCalled) startCourse();
			$.getJSON("ws/diary.php?method=load&userId=" + json.userId, function(datab){
				if (userdata==null) userdata = new Object();
				wbLog("diary.php retured " + JSON.stringify(datab));
				userdata.module = datab.diary;
			});
		}else{
			if (json.error=="Wrong password"){
				showError("login", json.error, 255);
			}else{
				showError("login", json.error, 155);
			}
		}
	}
)*/
	
function showLogin(){
	wbLog("Show login");
	$("#login").show();
	$("#forgotten").hide();
}

function loggedIn(){
	$("#container div.login").html(user.name);
	$("#login").hide();
	$("#nav-bar").hide();
	$("#nav-bottom").show();
	$("#container div.img-right").show();
	$("#container div.img-left").hide();
	$("#intro").show();
	$("#register").hide();
	$("#container div.nav_btns").show();
	$("#container div.login").css("right", 156);
}
		
$(document).ready(function(){
	wbLog("Document ready");
	preload();
	$.getJSON( "js/wellbeing_pam2.json", function( _data ) {
  		data = _data;
		if (loginInfo.loggedIn && loginInfo.registered){
			wbLog("loginInfo.loggedIn and registered");
			user = { id:loginInfo.id, name:loginInfo.userName };
			loggedIn();
			startCourse();
			$.getJSON("ws/diary.php?method=load&userId=" + user.id, function(data){
				if (userdata==null) userdata = new Object();
				wbLog("diary.php retured " + JSON.stringify(data));
				userdata.module = data.diary;
			});
		}else{
			selectModule(0);
		}
		wbLog("data bottomtabs count:" + data.bottomtabs.length);
  	});
	$('#loginForm').ajaxForm(function(datab) {
		while(datab.charAt(0)!="{") datab = datab.substr(1);
		console.log("loginForm " + datab); 
		var json = JSON.parse(datab);
		if (json.success){
			wbLog("loginForm retured " + datab);
			user = { id:json.userId, name:json.userName };
			loggedIn();
			if (startCourseCalled) startCourse();
			$.getJSON("ws/diary.php?method=load&userId=" + json.userId, function(datab){
				if (userdata==null) userdata = new Object();
				wbLog("diary.php retured " + JSON.stringify(datab));
				userdata.module = datab.diary;
			});
		}else{
			if (json.error=="Wrong password"){
				showError("login", json.error, 255);
			}else{
				showError("login", json.error, 155);
			}
		}
	});
	$('#registerForm').ajaxForm(function(datab) {
		console.log("registerForm " + datab); 
		var json = JSON.parse(datab);
		if (json.success){
			wbLog("registerForm retured " + datab);
			user = { id:json.userId, name:json.userName };
			loggedIn();
			startCourse();
			userdata = {};
			userdata.module = {};
		}else{
			showError("register", json.error, 42);
		}
	});
	$('#forgottenForm').ajaxForm(function(datab) {
		console.log("forgottenForm " + datab); 
		var json = JSON.parse(datab);
		if (json.success){
			wbLog("forgottenForm retured " + datab);
			$("#forgotten div.message").text("Your password has been sent to your email address");
			$("#forgotten .submitBtn").text("Continue");
			$("#forgotten .submitBtn").show();
		}else{
			forgottenPressed();
			showError("forgotten", json.error, 155);
		}
	});
	$('#diaryForm').ajaxForm(function(datab) {
		console.log("diaryForm " + datab); 
		var json = JSON.parse(datab);
		wbLog("diaryForm retured " + datab);
		if (json.success){
			
		}
	});
	$( "#team_spinner" ).spinner();
	$("#wsas-slider").slider({min: 0, max: 10, value:5});
	$("#start-slider1").slider({min: 0, max: 10, value:5});
	$("#start-slider2").slider({min: 0, max: 10, value:5});
	$("#end-slider1").slider({min: 0, max: 10, value:5});
	$("#end-slider2").slider({min: 0, max: 10, value:5});
	
	var drag_box = $( "#drag_activity div.drag_box" );
	drag_data = { box:drag_box, left:drag_box.css("left"), top:drag_box.css("top") };
	drag_box.draggable({
      start: function() {
      },
      drag: function() {
      },
      stop: function() {
		  wbLog("End drag (" + drag_data.box.css("left") + ", " + drag_data.box.css("top") + ")");
		  var pos = drag_data.box.position();
		  var showFeedback = false;
		  if (pos.top>380 && pos.top<509){
			  if (pos.left>-25 && pos.left<192){
				  //Yes
				  drag_data.box.hide();
				  showFeedback = drag_data.feedbackOnYes; 
			  }else if (pos.left>569 && pos.left<740){
				  drag_data.box.hide();
				  showFeedback = !drag_data.feedbackOnYes; 
			  }
			  
			  if (showFeedback){
				  var popup = $("#drag_activity div.popup");
				  popup.css("left", 960);
				  popup.animate({left:75}, 500, "easeOutCubic");
				  popup.show();
			  }else{
				  drag_data.questionIdx++;
				  if (drag_data.questionIdx<drag_data.content.questions.length){
				  	setTimeout(initDragQuestion, 500);
				  }else{
					setTimeout(endDragActivity, 500);
				  }
			  }
		  }
		  drag_data.box.css("left", drag_data.left );
		  drag_data.box.css("top", drag_data.top );
      }
    });
});

function show_questionaire(){
	var elm = $("#questionaire");
	elm.css("opacity", 0);
	elm.show();
	elm.animate({opacity:1}, 500);
}

function close_questionaire(){
	var elm = $("#questionaire");
	elm.animate({opacity:0}, 500, function(){
		var elm = $("#questionaire");
		elm.hide();
	});
}

function closeIntro(){
	$("#intro_panel").animate({opacity:0}, 500, function(){
		$("#intro_panel").hide();
	});
	var tab = $("#nav-bottom a.tab" + selected.page);
	tab.height(55);
	tab.css("margin-top", 0);
}

function showQuiz(){
	$("div.login").css("right", 10);
	$("div.nav_btns").hide();
	$("#img_diary").hide();
	$("#quiz div.title").text("Congratulations " + user.name + "!");
	$("#quiz").show();
	$("#quiz div.next-btn").show();
	$("#quiz div.complete").hide();
	quiz.page = 0;
	quiz.results = new Array();
	updateQuiz();
}

function quizNext(){
	if (quiz.page<data.quiz.length){
		quiz.page++;
		updateQuiz();
	}
}

function updateQuiz(){
	quiz.locked = false;
	if (quiz.page==data.quiz.length){
		$("#quiz div.next-btn").hide();
		$("#quiz div.complete").show();
	}else{
		$("#quiz div.progress").text("Question " + (quiz.page+1) + " of " + data.quiz.length);
		$("#quiz div.question").text(data.quiz[quiz.page].question);
		for(var i=1; i<=3; i++){
			$("#answer" + i + " div.answer_bg").css("background-color", "#f9CFCD");
			$("#answer" + i + " div.answer_title").text(data.quiz[quiz.page].answers[i-1]);
			$("#answer" + i + " div.answer_tick").hide();
			var answer = $("#answer" + i);
			answer.css("left", 1024);
			$("#answer" + i).animate({left:17}, 300);
		}
	}
}

function answerPressed(idx){
	if (quiz.locked) return;
	quiz.answer = idx;
	for(var i=1; i<=3; i++){
		if (i==idx){
			$("#answer" + i + " div.answer_bg").css("background-color", "#f98A84");
		}else{
			$("#answer" + i + " div.answer_bg").css("background-color", "#f9CFCD");
		}
	}
}

function quizNextPressed(){
	var idx = quiz.answer;
	var tick = $("#answer" + idx + " div.answer_tick");
	if (data.quiz[quiz.page].correct == (idx-1)){
		tick.css("background-image", "url(images/tick.png)");
		quiz.results[idx-1] = true;
	}else{
		tick.css("background-image", "url(images/cross.png)");
		quiz.results[idx-1] = false;
	}
	tick.show();
	quiz.locked = true;
	setTimeout(quizNext, 2000);
}

function endQuiz(){
	$("#quiz").hide();
	$("#course_end div.title").text("Congratulations " + user.name + "!");
	$("#course_end").show();
}

function showEndCourse(){
	$.post("ws/userdata.php", { method:"get_start_data", userId:user.id }, function(datab){
		while(datab.charAt(0)!="{") datab = datab.substr(1);
		wbLog("get_start_data returned " + datab);
		var json = JSON.parse(datab);
		if (json.success){
			questionaire = { index:0, start:false, answers:json.data };
		}else{
			questionaire = { index:0, start:false, answers:[5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5] };
		}
		updateQuestionaire();
		$("#img_diary").hide();
		$("#course_end div.title").text("Congratulations " + user.name + "!");
		$("#course_end").show();
		$("#course_end div.download_certificate").hide();
	});
}

function selectModule(idx, page){
	wbLog("selectModule " + idx);
	if (selected.module!=-1){
		$("#nav-bar a.tab" + selected.module).height(50);
		$("#nav-bar a.tab" + selected.module + ":hover").css("height", 55);
	}
	$("#nav-bar a.tab" + idx).height(60);
	$("#nav-bar a.tab" + selected.module + ":hover").css("height", 60);
	var tabData = data.bottomtabs[idx];
	for(var i=1; i<=5; i++){
		var elm = $("#nav-bottom a.tab" + i);
		elm.css("background-color", tabData.labels[i-1].colour);
		elm.html(tabData.labels[i-1].label);
	}
	var nav = $("#nav-bottom");
	nav.css("bottom", -60);
	nav.stop();
	nav.animate({bottom:0}, 500, "easeOutCubic" );
	var div = $("#start_course");
	div.css("bottom", -60);
	div.animate({bottom:0}, 500, "easeOutCubic" );
	selected.module = idx;
	var startBtn = $("#start_course");
	if (idx>0){
		if (page==null) page=0;
		if (userdata.state == null) userdata.state = new Object();
		userdata.state.module = idx;
		selectPage(page);
		startBtn.hide();
	}else{
		$('#register').hide();
		startBtn.show();
	}
}

function selectPage(idx){	
	if (selected.module==0 && idx==5){
		if ($("#start_course").css("display")!="none") startCourse();
		return;
	}
	var content;
	if (selected.module==0){
		 content = data.content[selected.module].pages[idx-1];
	}else{
		content = data.content[selected.module].pages[idx];
	}
	var tab;
	if (selected.page>0){
		 tab = $("#nav-bottom a.tab" + selected.page);
		 tab.height(55);
		 tab.css("margin-top", 0);
	}
	if (idx>0){
		tab = $("#nav-bottom a.tab" + idx);
		tab.height(65);
		tab.css("margin-top", -10);
	}
	
	$("#learning_panel").hide();
	$("#your_turn_panel").hide();
	$("#question-buttons").hide();
	$("#drag_activity").hide();
	$("#pairs").hide();
	$("#wsas").hide();
	
	if (selected.module==0){
		//This is a intro-panel 
		var panel = $("#intro_panel");
		panel.css("opacity", 0);
		panel.show();
		panel.animate({opacity:1}, 500);
		$("#intro_panel div.text").html(content.main);
		$("#intro_panel div.pic img").attr("src", "images/" + content.image);
		$("#intro_panel div.pic img").attr("alt", content.alt);
		$("#intro_panel div.img-right img").attr("src", "images/" + content.image);
		$("#intro_panel div.pointer").css("left", (idx-1) * 192 + 60);
	//}else if (selected.module==6 && idx==5){
	//	showQuiz();
	}else{
		$("#intro_panel").hide();
		var nav = $("#content-nav");
		
		if (Array.isArray(content.title)){
			$("#container div.subtitle").html(content.title[0]);
			selected.subpage = 0;
			nav.show();
			$("#content-nav div.back-btn").hide();
			$("#content-nav div.next-btn").show();
		}else{
			$("#container div.subtitle").html(content.title);
			nav.hide();
		}
		if (Array.isArray(content.main)){
			$("#container div.pagecontent div.content_main").html(content.main[0]);
			selected.subpage = 0;
		}else{
			$("#container div.pagecontent div.content_main").html(content.main);
		}
		
		var imgright = $("#container div.img-right");
		var imgleft = $("#container div.img-left");
		var showImageOnLeft = (content.imageleft!=null && content.imageleft);
		if ((content.panel!=null && content.panel.type=="your_turn") || content.panels!=null) showImageOnLeft = true;
		
		if (content.image==""){
			imgright.hide();
			imgleft.hide();
		}else if (showImageOnLeft){
			$("#container div.img-left img").attr("src", "images/" + content.image);
			$("#container div.img-left img").attr("alt", content.alt);	
			imgleft.show();
			imgleft.css("left", -300);
			imgleft.animate({left:0}, 500, "easeOutCubic");
			imgright.hide();
		}else{
			$("#container div.img-right img").attr("src", "images/" + content.image);
			$("#container div.img-right img").attr("alt", content.alt);	
			imgright.show();
			imgright.css("right", -300);
			imgright.animate({right:0}, 500, "easeOutCubic");
			imgleft.hide();
		}
		
		if (content.panel!=null){
			showPanel(content.panel);
		}else if (content.panels!=null){
			for(var i=0; i<content.panels.length; i++){
				showPanel(content.panels[i]);
			}
		}
		
		var pagecontent = $("#container div.pagecontent");
		pagecontent.show();
		pagecontent.css("opacity", 0);
		pagecontent.animate({opacity:1}, 500);
		
		if (content.interactive!=null){
			if (content.interactive.type=="drag"){
				initDragActivity(content);
			}else if(content.interactive.type=="question-buttons"){
				initQuestionButtons(content.interactive.buttons);
			}else if(content.interactive.type=="pairs"){
				//initPairs(content.interactive.pairs);
			}
		}
	}
	
	$("div.content_main").show();
	selected.page = idx;
	if (userdata.state == null) userdata.state = new Object();
	userdata.state.page = selected.page;
	if (selected.module>0 && !(selected.module==6 && idx==5)) saveState();
}

function selectSubPage(next){
	var content = data.content[selected.module].pages[selected.page];
	var ok = false;
	
	if (next){
		if (selected.subpage<(content.title.length-1)){
			selected.subpage++;
			ok = true;
		}
	}else{
		if (selected.subpage>0){
			selected.subpage--;
			ok = true;
		}
	}
	
	if (ok){
		if (content.panel!=null && content.panel.type=="your_turn" && content.panel.main.constructor === Array) showPanel(content.panel, selected.subpage);
		$("#container div.subtitle").html(content.title[selected.subpage]);
		$("#container div.content_main").html(content.main[selected.subpage]);
		if (selected.subpage>0){
			$("#content-nav div.back-btn").show();
		}else{
			$("#content-nav div.back-btn").hide();
		}
		if (selected.subpage<(content.title.length-1)){
			$("#content-nav div.next-btn").show();
		}else{
			$("#content-nav div.next-btn").hide();
		}
	}
}

function selectSubPageWithIndex(idx){
	selected.subpage = idx;
	selectSubPage(false);
}

function showPopup(module, page, idx){
	var panel = $("#popup_panel");
	panel.css("opacity", 0);
	panel.show();
	panel.animate({opacity:1}, 500);
	
	if (idx==null){
		var content = data.content[module].pages[page].popup;
	}else{
		var content = data.content[module].pages[page].popups[idx];
	}
	
	$("#popup_panel div.text").html(content.text);
	$("#popup_panel div.pic img").attr("src", "images/" + content.image);
	$("#popup_panel div.pic img").attr("alt", content.alt);
	$("#popup_panel div.img-right img").attr("src", "images/" + content.image);
}

function closePopup(){
	$("#popup_panel").animate({opacity:0}, 500, function(){
		$("#popup_panel").hide();
	});
}

function initPairs(pairs){
	if (pairs==null){
		var content = data.content[selected.module].pages[selected.page];
		pairs = content.interactive.pairs;
	}
	interactive_data = new Array();
	for(var i=0; i<6; i++){
		var btnLeft = $("#pair" + i + "_0");
		var btnRight = $("#pair" + i + "_1");
		if (Math.random()>0.5){
			interactive_data.push({correct:"left", selected:-1});
			//Left is correct
			btnLeft.text(pairs[i][0]);
			btnRight.text(pairs[i][1]);
		}else{
			interactive_data.push({correct:"right", selected:-1});
			//Right is correct
			btnLeft.text(pairs[i][1]);
			btnRight.text(pairs[i][0]);
		}
		if (i%2!=0){
			btnLeft.css("background", "#5DBDFF");
			btnRight.css("background", "#5DBDFF");
		}else{
			btnLeft.css("background", "#91D2FF");
			btnRight.css("background", "#91D2FF");
		}
		$("#pair" + i + "_tickcross").hide();
	}
	
	var elm = $("#pairs");
	elm.css("opacity", 0);
	elm.animate({opacity:1}, 500);
	elm.show();
	
	$("div.content_main").html("At first glance, which one of each pair do you think may have lower wellbeing? Click the buttons and then press DONE.");
}

function pairsPressed(row, col){
	var btnLeft = $("#pair" + row + "_0");
	var btnRight = $("#pair" + row + "_1");
	if (row%2!=0){
		if (col!=0) btnLeft.css("background", "#5DBDFF");
		if (col!=1) btnRight.css("background", "#5DBDFF");
	}else{
		if (col!=0) btnLeft.css("background", "#91D2FF");
		if (col!=1) btnRight.css("background", "#91D2FF");
	}
	if(col==0){
		btnLeft.css("background", "#00CC66");
	}else{
		btnRight.css("background", "#00CC66");
	}
	interactive_data[row].selected = col;
}

function pairsDonePressed(){
	for(var i=0; i<6; i++){
		var elm = $("#pair" + i + "_tickcross");
		if ((interactive_data[i].selected==0 && interactive_data[i].correct=="left") || (interactive_data[i].selected==1 && interactive_data[i].correct=="right")){
			elm.css("background-image", "url(images/tick.png)");
		}else{
			elm.css("background-image", "url(images/cross.png)");
		}
		elm.show();
	}
}

function showQuestionBoxPopup(idx){
	var popup = $("#question-buttons div.popup");
	var text = $("#question-buttons div.text");
	text.html(interactive_data[idx]);
	text.scrollTop(0);
	popup.stop();
	popup.css("opacity", 0);
	popup.show();
	popup.animate({opacity:1}, 500);
}
function closeQuestionBoxPopup(){
	var popup = $("#question-buttons div.popup");
	popup.hide();
}
function initQuestionButtons(buttons){
	var bgcol = data.bottomtabs[selected.module].labels[0].colour;
	var textcol = data.bottomtabs[selected.module].labels[3].colour;
	var elm = $("#question-buttons div.buttons");
	var width = elm.width();
	var padding = (width - 76 * buttons.length)/(buttons.length-1);
	str = "<ul>";
	for(var i=0; i<buttons.length; i++){
		if (i<(buttons.length-1)){
			str += "<li><a href=\"javascript:showQuestionBoxPopup(" + i + ")\" title=\"Show Popup\" style=\"margin-right:" + padding + "px; background:" + bgcol + "; color:" + textcol + ";\"><img src=\"images/pointer.png\" /></a></li>";
		}else{
			str += "<li><a href=\"javascript:showQuestionBoxPopup(" + i + ")\" title=\"Show Popup\" style=\"margin-right:0px; background:" + bgcol + "; color:" + textcol + ";\"><img src=\"images/pointer.png\" /></a></li>";
		}
	}
	str += "</ul>";
	elm.html(str);
	
	elm = $("#question-buttons");
	elm.css("bottom", -20); 
	elm.animate({bottom: 70}, 500, "easeOutCubic");
	elm.show();
	
	elm = $("#question-buttons div.popup");
	elm.hide();
	
	interactive_data = buttons;
}

function initDragActivity(content){
	$("#drag_activity div.title").html(content.title);
	$("#drag_activity div.maintext").html(content.main);
	drag_data.questionIdx = 0;
	drag_data.content = content.interactive;
	initDragQuestion();
	
	var drag = $("#drag_activity");
	drag.show();
	drag.css("opacity", 0);
	drag.animate({opacity:1}, 500);
	
	$("#img_diary").hide();
}

function initDragQuestion(){
	var question = drag_data.content.questions[drag_data.questionIdx];
	$("#drag_activity div.drag_text").html(question.text);
	$("#drag_activity div.text").html(question.feedback);
	drag_data.feedbackOnYes = (question.feedbackOnYes!=null && question.feedbackOnYes);
	drag_data.box.css("opacity", 0);
	drag_data.box.animate({opacity:1}, 500);
	drag_data.box.show();
}

function closeDragPopup(){
	var popup = $("#drag_activity div.popup");
	popup.stop();
	popup.animate({left:960}, 500, "easeInCubic", function(){
		$("#drag_activity div.popup").hide();
	});
	drag_data.questionIdx++;
	if (drag_data.questionIdx<drag_data.content.questions.length){
		initDragQuestion();
	}else{
		endDragActivity();
	}
}

function endDragActivity(){
	$("#drag_activity").animate({opacity:0}, 500, function(){
		$("#drag_activity").hide();
		$("#img_diary").show();
		if (drag_data.content.end!=null){
			console.log("endDragActivity: Setting page content");
			$("#container div.pagecontent div.subtitle").html(drag_data.content.end.title);
			$("#container div.pagecontent div.content_main").html(drag_data.content.end.main);	
		}else{
			console.log("endDragActivity: no drag end data");
		}
	});
}

function saveState(){
	$.post("ws/userdata.php", { method:"set_state", userId:user.id, data:JSON.stringify(userdata.state) }, function(datab){
		//while(data.charAt(0)!="{") data = data.substr(1);
		wbLog("saveState returned " + datab);
	});
}

function showPanel(panel, idx){
	var elm = $("#" + panel.type + "_panel");
	elm.show();
	if (panel.type=="learning"){
		elm.css("right", 0);
		elm.animate({right:330}, 500, "easeOutCubic");
		$("#learning_panel div.text").html(panel.main);
		var col = data.bottomtabs[selected.module].labels[0].colour;
		if (col!=null) $("#learning_panel div.bg").css("background", col);
	}else if (panel.type=="your_turn"){
		if (panel.main.constructor === Array){
			idx = (idx==null) ? 0 : idx;
			$("#your_turn_panel div.text").html(panel.main[idx]);
		}else{
			$("#your_turn_panel div.text").html(panel.main);
		}
		var col = data.bottomtabs[selected.module].labels[2].colour;
		if (col!=null) $("#your_turn_panel").css("background", col);
		var tip = $("#tip_panel");
		if (panel.tip==null){
			tip.hide();
		}else{
			tip.show();
			$("#tip_panel div.text").html(panel.tip);
		}
		elm.css("right", -330);
		elm.animate({right:0}, 500, "easeOutCubic");
	}
}

function startCourse(){
	wbLog("startCourse");
	startCourseCalled = true;
	if (user==null){
		login();
	}else{
		$.post("ws/userdata.php", { method:"get_state", userId:user.id }, function(_data){
			while(_data.charAt(0)!="{") _data = _data.substr(1);
			wbLog("startCourse get_state " + _data);
			var json = JSON.parse(_data);
			//json.success = false;
			if (json.success && !showQuestionaire){
				if (userdata==null) userdata = new Object();
				userdata.state = json.data;
				if (userdata.state==null) userdata.state = new Object();
				if (userdata.state.module==null) userdata.state.module = 1;
				$("#intro").hide();
				var navbar = $("#nav-bar");
				navbar.css("opacity", 0);
				navbar.show();
				navbar.animate({opacity:1}, 500);
				var diary = $("#img_diary");
				diary.css("right", -60);
				diary.animate({right:10}, 500, "easeOutCubic");
				diary.show();
				if (userdata.state.module==null){
					selectModule(1);
				}else{
					selectModule(userdata.state.module, userdata.state.page);
				}
			}else{
				questionaire = { index:0, start:true, answers:new Array() };
				var elm = $("#course_start");
				elm.css("opacity", 0);
				elm.animate({opacity:1}, 500);
				elm.show();
				$("#course_start div.start_module").hide();
				$("#course_start div.hello").text("Hello " + user.name);
				updateQuestionaire();
			}
		});
	}
}

function questionaireNextPressed(){
	wbLog("questionaireNextPressed");
	if (questionaire.index!=(data.questionaire-1)){
		if (questionaire.start){
			var sld = $("#start-slider1");
			questionaire.answers.push(sld.slider("value"));
		}else{
			var sld = $("#end-slider1");
			questionaire.answers.push(sld.slider("value"));
		}
		questionaire.index++;
		updateQuestionaire();
	}
}

function updateQuestionaire(){
	wbLog("updateQuestionaire index:" + questionaire.index + " count:" + data.questionaire.length);
	var question = data.questionaire[questionaire.index];
	if (questionaire.start){
		var heading = $("#course_start div.subheading");
		heading.text(question.heading + " - " + (questionaire.index+1) + " of " + data.questionaire.length);
		var txt = $("#course_start div.slider_title");
		txt.text(question.title);
		var next_btn = $("#course_start div.next-btn");
		var left = $("#course_start div.slider_label1");
		var right = $("#course_start div.slider_label2");
		if (question.left==null){
			left.text("very unconfident");
		}else{
			left.text(question.left);
		}
		if (question.right==null){
			right.text("very confident");
		}else{
			right.text(question.right);
		}
		if (questionaire.index==(data.questionaire.length-1)){
			next_btn.hide();
			$("#course_start div.start_module").show();
			$.post("ws/userdata.php", { method:"set_start_data", userId:user.id, data:JSON.stringify(questionaire.answers) }, function(datab){
				//while(data.charAt(0)!="{") data = data.substr(1);
				wbLog("updateQuestionaire saved start_data " + JSON.stringify(datab));
			});
		}else{
			next_btn.show();
			//next_btn.html("<a href=\"javascript:questionaireNextPressed();\">Next</a>");
		}
		var sld = $("#start-slider1");
		sld.slider("value", 5);
	}else{
		var heading = $("#course_end div.subheading");
		heading.text(question.heading + " - " + (questionaire.index+1) + " of " + data.questionaire.length);
		var txt = $("#course_end div.slider_title");
		txt.text(question.title);
		var next_btn = $("#course_end div.next-btn");
		var left = $("#course_end div.slider_label1");
		var right = $("#course_end div.slider_label2");
		if (question.left==null){
			left.text("very unconfident");
		}else{
			left.text(question.left);
		}
		if (question.right==null){
			right.text("very confident");
		}else{
			right.text(question.right);
		}
		if (questionaire.index==(data.questionaire.length-1)){
			next_btn.hide();
			$("#course_end div.download_certificate").show();
			downloadCertificate();
			$.post("ws/userdata.php", { method:"set_end_data", userId:user.id, data:JSON.stringify(questionaire.answers) }, function(datab){
				//while(data.charAt(0)!="{") data = data.substr(1);
				wbLog("updateQuestionaire saved end_data " + JSON.stringify(datab));
			});
		}else{
			next_btn.show();
			//next_btn.html("<a href=\"javascript:questionaireNextPressed();\">Next</a>");
		}
		var sld = $("#end-slider1");
		sld.slider("value", questionaire.answers[questionaire.index]);
	}
}

function changePage(){
	if (userdata.state == null) userdata.state = new Object();
	selectPage(userdata.state.page);
}

function startCourseRollover(){
	var tab = $("#nav-bottom a.tab5");
	tab.height(60);
	tab.css("margin-top", -5);
	var div = $("#start_course");
	div.css("bottom", 5);
}

function startCourseRollout(){
	var tab = $("#nav-bottom a.tab5");
	tab.height(55);
	tab.css("margin-top", 0);
	var div = $("#start_course");
	div.css("bottom", 0);
}

function login(){
	wbLog("login pressed");
	
	$("#nav-bar").hide();
	$("#nav-bottom").hide();
	$("#container div.img-right").hide();
	$("#container div.img-left").hide();
	$("#intro").hide();
	$("#register").hide();
	var login = $("#login");
	login.css("opacity", 0);
	login.animate({opacity:1}, 500);
	login.show();
}

function logout(){
	$("#container div.login").html("<a href=\"javascript:login();\" title=\"Login\">Login</a> or <a href=\"javascript:register();\" title=\"Register\">Register</a>");
	$("#nav-bar").hide();
	$("#nav-bottom").show();
	$("#container div.img-right img").attr("src", "images/3girls.jpg");
	$("#container div.pagecontent").hide();
	$("#img_diary").hide();
	$("#intro").show();
	$("#register").hide();
	$("#login").hide();
	$("#your_turn_panel").hide();
	$("#learning_panel").hide();
	$("#container div.login").css("right", 10);
	$("#container div.nav_btns").hide();
	$("#course_start").hide();
	$("#course_end").hide();
	$("#quiz").hide();
	$("#wsas").hide();
	
	user = null;
	userdata = null;
	startCourseCalled = false;
	if (selected.module>0){
		$("#nav-bar a.tab" + selected.module).height(50);
		$("#nav-bar a.tab" + selected.module + ":hover").css("height", 55);
	}
	if (selected.page>0){
		 tab = $("#nav-bottom a.tab" + selected.page);
		 tab.height(55);
		 tab.css("margin-top", 0);
	}
	selectModule(0);
}

function rewindPressed(){
	selectModule(1, 0);
}

function backPressed(){
	if (selected.page==0){
		if (selected.module>1) selectModule(selected.module-1, data.content[selected.module-1].pages.length-1);
	}else{
		selectPage(selected.page-1);
	}
}

function nextPressed(){
	if (selected.page==(data.content[selected.module].pages.length-1)){
		if (selected.module<6) selectModule(selected.module+1, 0);
	}else{
		selectPage(selected.page+1);
	}
}

function register(){
	wbLog("register pressed");
	$("#nav-bar").hide();
	$("#nav-bottom").hide();
	$("#container div.img-right").hide();
	$("#container div.img-left").hide();
	$("#intro").hide();
	$("#login").hide();
	var register = $("#register");
	register.css("opacity", 0);
	register.animate({opacity:1}, 500);
	register.show();
	$("#register1").show();
	$("#register2").hide();
	$("#register3").hide();
	$("#register4").hide();
	$("#register5").hide();
	$("#continue-btn").show();
	$("#submit-btn").hide();
	registerPage = 1;
}

function makeDatepicker(){
	$('#register_dob').datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: '1920:2005',
    });
	$('#register_dob').datepicker("option", "dateFormat", "dd/mm/yy");
}

function registerBack(){
	registerPage -= 2;
	registerNextPage();
}

function registerNextPage(){
	var form = document.getElementById("registerForm");
	if (!registerValidate(form, registerPage)) return;
	registerPage++;
	switch(registerPage){
		case 1:
		$("#register1").show();
		$("#register2").hide();
		$("#register div.back").hide();
		break;
		case 2:
		$("#register1").hide();
		$("#register2").show();
		$("#register3").hide();
		$("#register div.back").show();
		setTimeout(makeDatepicker, 20);
		break;
		case 3:
		$("#register2").hide();
		$("#register3").show();
		$("#register4").hide();
		break;
		case 4:
		$("#register3").hide();
		$("#register4").show();
		$("#register5").hide();
		break;
		case 5:
		$("#register4").hide();
		$("#register5").show();
		$("#continue-btn").hide();
		$("#submit-btn").show();
		break;
	}
}

function showError(panel, error, top){
	var elm = $("#" + panel + " div.error");
	elm.css("top", top);
	elm.text(error);
	elm.show();
	elm.css("opacity", 0);
	elm.animate({opacity:1}, 500);
	errorInfo.elm = elm;
	if (errorInfo.id!=null) clearTimeout(errorInfo.id);
	errorInfo.id = setTimeout(clearError, 3000);
}

function clearError(){
	errorInfo.elm.hide();
	errorInfo.id = null;
	errorInfo.elm = null;
}

function loginValidate(form){
	var result = true;
	if (form.email.value==""){
		showError("login", "Please enter your email", 155);
		result = false;
	}else if (form.password.value==""){
		showError("login", "Please enter your password", 257);
		result = false;
	}
	email = form.email.value;
	return result;
}

function forgottenValidate(form){
	var result = true;
	if (form.email.value==""){
		showError("forgotten", "Please enter your email", 155);
		result = false;
	}
	email = form.email.value;
	if (result){
		$("#forgotten div.label1").hide();
		$("#forgotten div.input1").hide();
		$("#forgotten .submitBtn").hide();
		$("#forgotten div.message").text("Sending...");
	}
	return result;
}

function registerValidate(form, page){
	var result = true;
	switch(page){
		case 1:
		if (form.firstname.value==""){
			showError("register", "Please enter your first name", 42);
			result = false;
		}else if (form.address1.value==""){
			showError("register", "Please enter your address", 229);
			result = false;
		}else if (form.city.value==""){
			showError("register", "Please enter your city", 323);
			result = false;
		}else if (form.county.value==""){
			showError("register", "Please enter your county", 370);
			result = false;
		}else if (form.postcode.value==""){
			showError("register", "Please enter your postcode", 417);
			result = false;
		}
		email = form.email.value;
		break;
		case 2:
		if (form.your_number.value==""){
			showError("register", "Please enter your first phone number", 118);
			result = false;
		}else if (form.your_dob.value==""){
			showError("register", "Please enter your date of birth", 166);
			result = false;
		}
		break;
		case 3:
		break;
		case 4:
		if (form.you_jobtitle.value==""){
			showError("register", "Please enter your job title", 85);
			result = false;
		}
		break;
		case 5:
		if (form.experience_hear.value==""){
			showError("register", "Please enter how you heard about this training", 85);
			result = false;
		}else if (form.experience_experience.value==""){
			showError("register", "Please enter your experience", 172);
			result = false;
		}else if (form.experience_qualifications.value==""){
			showError("register", "Please enter your qualifications", 263);
			result = false;
		}
		break;
	}
	return result;
}

function showDiary(){
	var diary = $("#diary");
	diary.css("opacity", 0);
	diary.show();
	$("#diary div.title").text("Module " + selected.module);
	$("#diary div.tabs").css("background-image", "url(images/diary_tabs000" + selected.module + ".png)");
	var textleft = "";
	var textright = "";
	if (userdata!=null && userdata.module!=null && userdata.module[selected.module]!=null){
		textleft = userdata.module[selected.module].textleft;
		textright = userdata.module[selected.module].textright;
	}
	var form = document.getElementById("diaryForm");
	form.textleft.value = textleft;
	form.textright.value = textright;
	diary.animate({opacity:1}, 500);
}

function closeDiary(){
	if (userdata==null) userdata = { module:new Array() };
	if (userdata.module==null) userdata.module = new Array();
	var form = document.getElementById("diaryForm");
	userdata.module[selected.module] = { textleft:form.textleft.value, textright:form.textright.value };
	$.post("ws/diary.php", { userId:user.id, method:"save", diary:JSON.stringify(userdata.module) }, function(datab){
		//while(data.charAt(0)!="{") data = data.substr(1);
		wbLog("Saved diary returned " + datab);
	});
	var diary = $("#diary");
	diary.animate({opacity:0}, 500, function(){
		$("#diary").hide();
	});
}

function startModule(){
	var different = $("#start-slider1").slider("value");
	var sad = $("#start-slider2").slider("value");
	var start_data = { different:different, sad:sad };
	wbLog("startModule " + JSON.stringify(start_data));
	$.post("ws/userdata.php", { method:"start_course", userId:user.id, data:JSON.stringify(start_data) }, function(datab){
		//while(data.charAt(0)!="{") data = data.substr(1);
		wbLog("start_course returned " + datab);
		startCourse();
	});
	$("#course_start").hide();
}

function showWSAS(){
	$("#wsas div.progress").show();
	$("#wsas div.slider1").show();
	$("#wsas div.back-btn").show();
	$("#wsas div.skip-btn").show();
	$("#wsas div.next-btn").show();
	$("#wsas div.next-btn a").text("Next");
	$("#wsas div.feedback").hide();
	$("#wsas").show();
	wsas.page = 0;
	updateWSAS();
}

function nextWSAS(next){
	var content = data.content[8].pages[5].wsas;
	var slider = $("#wsas-slider");
	wsas.choices[wsas.page] = slider.slider("option", "value");
	
	if (next){
		if (wsas.page==content.length){
			//$("#wsas").hide();
			//showEndCourse();
			selectModule(8, 5);
			selectSubPageWithIndex(2);
			return;
		}
		if (wsas.page<content.length) wsas.page++;
	}else{
		if (wsas.page>0) wsas.page--;
	}
	
	updateWSAS();
}

function updateWSAS(){
	if (wsas.page==data.content[8].pages[5].wsas.length){
		$("#wsas div.progress").hide();
		$("#wsas div.slider1").hide();
		$("#wsas div.back-btn").hide();
		$("#wsas div.skip-btn").hide();
		$("#wsas div.next-btn a").text("Done");
		var feedback = $("#wsas div.feedback");
		var total = 0;
		for(var i=0; i<data.content[8].pages[5].wsas.length; i++){
			total += wsas.choices[i];
		}
		var score = Math.floor( (total/(data.content[8].pages[5].wsas.length * 10)) * 100 );
		feedback.html("Thank you for rating your wellbeing.<br>Your WSAS score is " + score + "%");
		feedback.show();
	}else{
		var content = data.content[8].pages[5].wsas[wsas.page];
		var text = $("#wsas div.slider_title");
		var slider = $("#wsas-slider");
		text.text(content);
		slider.slider('option', 'value', wsas.choices[wsas.page]);
		var progress = $("#wsas div.progress");
		progress.text("Question " + (wsas.page + 1) + " of " + data.content[8].pages[5].wsas.length);
		if (wsas.page>0){
			$("#wsas div.back-btn").show();
		}else{
			$("#wsas div.back-btn").hide();
		}
	}
}

function downloadCertificate(){
	if (loginInfo!=null && loginInfo.email!=null) email = loginInfo.email;
	$.post( "fpdf/send-certificate.php", { name:user.name, email:email }, function( datab ) {
		//while(data.charAt(0)!="{") data = data.substr(1);
	  	wbLog( datab.success ); 
	 	if (datab.success){
			alert("Your certificate has been sent to your email address.");
		}else{
			alert("There was a problem sending your certificate please try later. " + datab.error);
		}
	}, "json");
}