<?
//Dashboard actions
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require_once('connect.php');

$method = (empty($_REQUEST['method'])) ? '': $_REQUEST['method'];
$data = array();
switch($method){
	case 'get_user':
	getUser();
	break;
	case 'make_full':
	makeFull();
	break;
	default:
	$data['success'] = false;
	$data['error'] = 'No supported method';
	break;
}

mysqli_close($conn);

echo json_encode($data);
	
	
function getUser(){
	global $conn;
	global $data;
	
	$id = (empty($_REQUEST['user_id'])) ? 0: $_REQUEST['user_id'];
	
	$tables = [ 'users', 'setting', 'experience', 'you' ];
	
	foreach($tables as $table){
		if ($table=='users'){
			$sql = "SELECT * FROM $table WHERE id=$id";
		}else{
			$sql = "SELECT * FROM $table WHERE userId=$id";
		}
		$result = mysqli_query($conn, $sql);
		if (!$result){
			$data = array();
			$data['success'] = false;
			$data['error'] = 'SQL Error:'.mysqli_error($conn).' '.$sql;
			return;
		}
		
		$row = mysqli_fetch_assoc($result);
		
		$data[$table] = $row;
	}
	
	$sql = "SELECT * FROM userdata WHERE userId=$id";
	$result = mysqli_query($conn, $sql);
	if (!$result){
		$data = array();
		$data['success'] = false;
		$data['error'] = 'SQL Error:'.mysqli_error($conn).' '.$sql;
		return;
	}
	$userdata = array();
	
	while($row=mysqli_fetch_assoc($result)){
		$userdata[] = $row;
	}
	
	$data['data'] = $userdata;
	
	$data['success'] = true;
}

function makeFull(){
	global $conn;
	global $data;
	
	$id = (empty($_REQUEST['user_id'])) ? 0: $_REQUEST['user_id'];
	$sql = "UPDATE users SET full=1 WHERE id=$id";
	$result = mysqli_query($conn, $sql);
	if (!$result){
		$data = array();
		$data['success'] = false;
		$data['error'] = 'SQL Error:'.mysqli_error($conn).' '.$sql;
	}else{
		$data['success'] = true;
		$data['message'] = "user upgraded";
	}
}
?>