<?
	require_once('connect.php');
	
	$method = (isset($_REQUEST['method'])) ? $_REQUEST['method'] : "";
	$userId = (isset($_REQUEST['userId'])) ? $_REQUEST['userId'] : "";
	
	if ($method==""){
		echo '{ "success":false, "error":"No method" }';
	}else if ($userId==""){
		echo '{ "success":false, "error":"No userId" }';
	}else{
		switch($method){
			case "load":
			loadDiary($userId);
			break;
			case "save":
			saveDiary($userId);
			break;
		}
	}
	
	mysqli_close($conn);
	
function loadDiary($userId){
	global $conn;
	
	$sql = "SELECT data FROM userdata WHERE userId=$userId AND type='diary'";
	$result = mysqli_query($conn, $sql);
	if (!$result){
		echo '{ "success":false, "error":"SQL error loading diary:'.mysqli_error($result).'" }';
	}else if (mysqli_num_rows($result)==0){
		echo '{ "success":false, "error":"No diary data found" }';
	}else{
		$row = mysqli_fetch_assoc($result);
		$diary = $row['data'];
		echo '{ "success":true, "diary":'.$diary.'}';
	}
}

function saveDiary($userId){
	global $conn;
	
	$diary = (isset($_REQUEST['diary'])) ? mysqli_real_escape_string($conn, $_REQUEST['diary']) : "";
	$sql = "SELECT id FROM userdata WHERE type='diary' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		echo '{ "success":false, "error":"SQL error reading existing data:'.mysqli_error($conn).'" }';
	}else if (mysqli_num_rows($result)==0){
		$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'diary', '$diary')";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error inserting:'.mysqli_error($conn).'" }';
		}else{
			echo '{ "success":true, "msg":"Diary inserted" }';
		}
	}else{
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$sql = "UPDATE userdata SET data='$diary' WHERE id=$id";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error updating:'.mysqli_error($result).'" }';
		}else{
			echo '{ "success":true, "msg":"Diary updated" }';
		}
	}
}
?>