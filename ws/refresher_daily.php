<?
	require_once('../PHPMailer/class.phpmailer.php');
	
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	require_once('connect.php');
	
	
	$sql = "SELECT id, userId FROM completed WHERE refresherSent=0 AND DATE_ADD(completeDate, INTERVAL 3 MONTH) > NOW()";
	$result = mysqli_query($conn, $sql);
	
	$json = new stdClass;
	
	if (!$result){
		$json->success = false;
		$json->error = "SQL Error: $sql ".mysqli_error($conn);
	}else{
		$count = 0;
		while($row = mysqli_fetch_assoc($result)){
			$id = $row['id'];
			$userId = $row['userId'];
			$sql = "SELECT email, firstname, lastname FROM users WHERE id=$userId";
			$resultb = mysqli_query($conn, $sql);
			if (!$resultb){
				$json->success = false;
				$json->error = "SQL Error: $sql ".mysqli_error($conn);
				break;
			}else{
				$rowb = mysqli_fetch_assoc($resultb);
				$name = $rowb['firstname'].' '.$rowb['lastname'];
				if (!sendEmail($rowb['email'], $name)) break;
				$sql = "UPDATE completed SET refresherSent=1 WHERE id=$id";
				$resultb = mysqli_query($conn, $sql);
				if (!$resultb){
					$json->success = false;
					$json->error = "SQL Error: $sql ".mysqli_error($conn);
					break;
				}
				$count++;
			}
		}
		
		if (empty($json->error)){
			$json->success = true;
			$json->message = "$count emails sent";
		}
	}
	
	echo json_encode($json);
	
	mysqli_close($conn);
	
function sendEmail($email, $name){
	global $json;
	
	$mail = new PHPMailer(); // defaults to using php "mail()"

	$body = 'It is now over 3 months since you completed the Wellbeing course. It would be very useful to take a refresher module. <a href="http://theinstituteofwellbeing.com">Click here</a>';
	
	$mail->AddReplyTo("no-reply@tiow.org","Wellbeing");
	$mail->SetFrom('no-reply@tiow.org', 'Institute of Wellbeing');
	$mail->AddAddress($email, $name);
	$mail->Subject = "Wellbeing refresher module is available";

	$mail->AltBody = "It is now over 3 months since you completed the Wellbeing course. It would be very useful to take a refresher module. http://theinstituteofwellbeing.com"; 
	$mail->MsgHTML($body);

	if(!$mail->Send()) {
		$json->success = false;
		$json->error = "Mailer Error: " . $mail->ErrorInfo;
		return false;
	} else {
		$json->success = true;
		$json->message = "Message sent!";
		return true;
	}
}
?>