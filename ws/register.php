<?
	require_once('connect.php');
	
	$firstname = (isset($_REQUEST['firstname'])) ? $_REQUEST['firstname'] : " ";
	$lastname = (isset($_REQUEST['lastname'])) ? $_REQUEST['lastname'] : " ";
	$email = (isset($_REQUEST['email'])) ? $_REQUEST['email'] : " ";
	$wpId = (isset($_REQUEST['wpId'])) ? $_REQUEST['wpId'] : " ";
	$password = (isset($_REQUEST['password'])) ? $_REQUEST['password'] : " ";
	$address1 = (isset($_REQUEST['address1'])) ? $_REQUEST['address1'] : " ";
	$address2 = (isset($_REQUEST['address2'])) ? $_REQUEST['address2'] : " ";
	$city = (isset($_REQUEST['city'])) ? $_REQUEST['city'] : " ";
	$county = (isset($_REQUEST['county'])) ? $_REQUEST['county'] : " ";
	$postcode = (isset($_REQUEST['postcode'])) ? $_REQUEST['postcode'] : " ";
	$your_number = (isset($_REQUEST['your_number'])) ? $_REQUEST['your_number'] : " ";
	$your_dob = (isset($_REQUEST['your_dob'])) ? $_REQUEST['your_dob'] : " ";
	if ($your_dob!=" "){
		//Convert to MySQL date
		$tokens = explode("/", $your_dob);
		if (count($tokens)==3) $your_dob = $tokens[2].'-'.$tokens[1].'-'.$tokens[0];
	}
	$supervisors_name = (isset($_REQUEST['supervisors_name'])) ? $_REQUEST['supervisors_name'] : " ";
	$supervisors_email = (isset($_REQUEST['supervisors_email'])) ? $_REQUEST['supervisors_email'] : " ";
	$supervisors_number = (isset($_REQUEST['supervisors_number'])) ? $_REQUEST['supervisors_number'] : " ";
	$early_name = (isset($_REQUEST['early_name'])) ? $_REQUEST['early_name'] : " ";
	$early_type = (isset($_REQUEST['early_type'])) ? $_REQUEST['early_type'] : " ";
	$early_clients = (isset($_REQUEST['early_clients'])) ? $_REQUEST['early_clients'] : " ";
	$early_practice = (isset($_REQUEST['early_practice'])) ? $_REQUEST['early_practice'] : " ";
	$you_jobtitle = (isset($_REQUEST['you_jobtitle'])) ? $_REQUEST['you_jobtitle'] : " ";
	$you_team = (isset($_REQUEST['you_team'])) ? $_REQUEST['you_team'] : 0;
	$you_teamsize = (isset($_REQUEST['you_teamsize'])) ? $_REQUEST['you_teamsize'] : 0;
	$you_motivation = (isset($_REQUEST['you_motivation'])) ? $_REQUEST['you_motivation'] : " ";
	$you_ambition = (isset($_REQUEST['you_ambition'])) ? $_REQUEST['you_ambition'] : " ";
	$you_progress = (isset($_REQUEST['you_progress'])) ? $_REQUEST['you_progress'] : " ";
	$experience_hear = (isset($_REQUEST['experience_hear'])) ? $_REQUEST['experience_hear'] : " ";
	$experience_experience = (isset($_REQUEST['experience_experience'])) ? $_REQUEST['experience_experience'] : " ";
	$experience_qualifications = (isset($_REQUEST['experience_qualifications'])) ? $_REQUEST['experience_qualifications'] : " ";
	
	$sql = "SELECT id FROM users WHERE email='$email'";
	$result = mysqli_query($conn, $sql);
		
	if (!$result){
		echo '{ "success":false, "error":"SQL error problem checking email:'.mysql_error($conn).' '.$sql.'" }';
	}else if (mysqli_num_rows($result)>0){
		echo '{ "success":false, "error":"A user with this email ('.$email.') already exists." }';
	}else{
		$registerTime = date ("Y-m-d H:i:s");
		
		$sql = "INSERT INTO users (firstname, lastname, email, wpId, registerTime, password, address1, address2, city, county, postcode, phone, dob) VALUES('$firstname', '$lastname', '$email', '$wpId', '$registerTime', '$password', '$address1', '$address2', '$city', '$county', '$postcode', '$your_number', '$your_dob')";
		$result = mysqli_query($conn, $sql);
			
		if (!$result){
			echo '{ "success":false, "error":"SQL error users table:'.mysqli_error($conn).' '.$sql.'" }';
		}else{
			$userId = mysqli_insert_id($conn);
			$sql = "INSERT INTO setting (userId, name, type, clients, practice) VALUES ( $userId, '$early_name', '$early_type', '$early_clients', '$early_practice')";
			$result = mysqli_query($conn, $sql);	
			if (!$result){
				echo '{ "success":false, "error":"SQL error setting table:'.mysqli_error($conn).' '.$sql.'" }';
			}else{
				$sql = "INSERT INTO experience (userId, hear, experience, qualifications) VALUES ( $userId, '$experience_hear', '$experience_experience', '$experience_qualifications')";
				$result = mysqli_query($conn, $sql);	
				if (!$result){
					echo '{ "success":false, "error":"SQL error experience table:'.mysqli_error($conn).' '.$sql.'" }';
				}else{
					$sql = "INSERT INTO supervisors (userId, name, email, phone) VALUES ( $userId, '$supervisors_name', '$supervisors_email', '$supervisors_number')";
					$result = mysqli_query($conn, $sql);	
					if (!$result){
						echo '{ "success":false, "error":"SQL error supervisors table:'.mysqli_error($conn).' sql:'.$sql.'" }';
					}else{
						if ($you_team == 'on') $you_team = 1;
						if ($you_teamsize=='') $you_teamsize = 0;
						$sql = "INSERT INTO you (userId, jobtitle, team, teamsize, motivation, ambition, progress) VALUES ( $userId, '$you_jobtitle', $you_team, $you_teamsize, '$you_motivation', '$you_ambition', '$you_progress')";
						$result = mysqli_query($conn, $sql);	
						if (!$result){
							echo '{ "success":false, "error":"SQL error you table:'.mysqli_error($conn).' sql:'.$sql.'" }';
						}else{
							session_start();
							$_SESSION['userId'] = $userId;
							echo '{ "success":true, "msg":"User registered", "userId":'.$userId.', "userName":"'.$firstname.' '.$lastname.'"}';
						}
					}
				}
			}
		}
	}
	
	mysqli_close($conn);
?>