<?
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
	date_default_timezone_set("Europe/London");
	
	require_once('connect.php');
	
	$method = (isset($_REQUEST['method'])) ? $_REQUEST['method'] : "";
	$userId = (isset($_REQUEST['userId'])) ? $_REQUEST['userId'] : "";
	
	if ($method==""){
		echo '{ "success":false, "error":"No method" }';
	}else if ($userId==""){
		echo '{ "success":false, "error":"No userId" }';
	}else{
		switch($method){
			case "start_course":
			startCourse($userId);
			break;
			case "get_state":
			getState($userId);
			break;
			case "set_state":
			setState($userId);
			break;
			case "set_start_data":
			setStartData($userId);
			break;
			case "set_end_data":
			setEndData($userId);
			setCourseComplete($userId);
			break;
			case "set_refresher_data":
			setRefresherData($userId);
			break;
			case "get_start_data":
			getStartData($userId);
			break;
			case "get_end_data":
			getEndData($userId);
			break;
		}
	}
	
	mysqli_close($conn);

function startCourse($userId){
	global $conn;
	
	$data = (isset($_REQUEST['data'])) ? mysqli_real_escape_string($conn, $_REQUEST['data']) : "";
	$sql = "SELECT id FROM userdata WHERE type='start_course' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		echo '{ "success":false, "error":"SQL error reading existing data:'.mysqli_error($conn).'" }';
	}else if (mysqli_num_rows($result)==0){
		$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'start_course', '$data')";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error inserting:'.mysqli_error($conn).'" }';
		}else{
			$tmp = array('started'=>true);
			$json = json_encode($tmp);
			$data = mysqli_real_escape_string($conn, $json);
			$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'state', '$data')";
			$result = mysqli_query($conn, $sql);
			if (!$result){
				echo '{ "success":false, "error":"SQL error inserting state:'.mysqli_error($conn).'" }';
			}else{
				echo '{ "success":true, "msg":"Data inserted", "data":'.$json.' }';
			}
		}
	}else{
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$sql = "UPDATE userdata SET data='$data' WHERE id=$id";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error updating:'.mysqli_error($conn).'" }';
		}else{
			$tmp = array('started'=>true);
			$json = json_encode($tmp);
			$data = mysqli_real_escape_string($conn, $json);
			$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'state', '$data')";
			$result = mysqli_query($conn, $sql);
			if (!$result){
				echo '{ "success":false, "error":"SQL error inserting state:'.mysqli_error($conn).'" }';
			}else{
				echo '{ "success":true, "msg":"Data inserted", "data":'.$json.' }';
			}
		}
	}
}

function setState($userId){
	global $conn;
	
	$data = (isset($_REQUEST['data'])) ? mysqli_real_escape_string($conn, $_REQUEST['data']) : "";
	$sql = "SELECT id FROM userdata WHERE type='state' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	//echo $sql;
	
	if (!$result){
		echo '{ "success":false, "error":"SQL error reading existing data:'.mysqli_error($conn).'" }';
	}else if (mysqli_num_rows($result)==0){
		$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'state', '$data')";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error inserting:'.mysqli_error($conn).'" }';
		}else{
			echo '{ "success":true, "msg":"Data inserted" }';
		}
	}else{
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$sql = "UPDATE userdata SET data='$data' WHERE id=$id";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error updating:'.mysqli_error($conn).'" }';
		}else{
			echo '{ "success":true, "msg":"Data updated" }';
		}
	}
}

function getState($userId){
	global $conn;
	
	$sql = "SELECT data FROM userdata WHERE type='state' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		echo '{ "success":false, "error":"SQL error reading existing state data:'.mysqli_error($conn).'" }';
	}else if (mysqli_num_rows($result)==0){
		echo '{ "success":false, "error":"No state data" }';
	}else{
		$row = mysqli_fetch_assoc($result);
		$data = $row['data'];
		if (empty($data)) $data = '{"started":false}';
		echo '{ "success":true, "data":'.$data.'}';
	}
}

function setStartData($userId){
	global $conn;
	
	$data = (isset($_REQUEST['data'])) ? mysqli_real_escape_string($conn, $_REQUEST['data']) : "";
	$sql = "SELECT id FROM userdata WHERE type='start' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		echo '{ "success":false, "error":"SQL error reading existing data:'.mysqli_error($conn).'" }';
	}else if (mysqli_num_rows($result)==0){
		$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'start', '$data')";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error inserting:'.mysqli_error($conn).'" }';
		}else{
			echo '{ "success":true, "msg":"Data inserted" }';
		}
	}else{
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$sql = "UPDATE userdata SET data='$data' WHERE id=$id";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			echo '{ "success":false, "error":"SQL error updating:'.mysqli_error($conn).'" }';
		}else{
			echo '{ "success":true, "msg":"Data updated" }';
		}
	}
}

function setEndData($userId){
	global $json;
	global $conn;
	
	$json = array();
	
	$data = (isset($_REQUEST['data'])) ? mysqli_real_escape_string($conn, $_REQUEST['data']) : "";
	$sql = "SELECT id FROM userdata WHERE type='end' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		$json['success'] = false;
		$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
	}else if (mysqli_num_rows($result)==0){
		$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'end', '$data')";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			$json['success'] = false;
			$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
		}else{
			setCourseComplete($userId);
		}
	}else{
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$sql = "UPDATE userdata SET data='$data' WHERE id=$id";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			$json['success'] = false;
			$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
		}else{
			setCourseComplete($userId);
		}
	}
	
	echo json_encode($json);
}

function setCourseComplete($userId){
	global $conn;
	global $json;
	
	$sql = "SELECT * FROM completed WHERE userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		$json['success'] = false;
		$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
		return;
	}else if (mysqli_num_rows($result)>0){
		$json['success'] = false;
		$json['error'] = 'User has already completed the course';
		return;
	}
	
	$sql = "INSERT INTO completed (userId) VALUES ($userId)";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		$json['success'] = false;
		$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
	}else{
		$json['success'] = true;
		$json['message'] = 'Course complete data set';
	}
}

function setRefresherData($userId){
	global $json;
	global $conn;
	
	$json = array();
	
	$data = (isset($_REQUEST['data'])) ? mysqli_real_escape_string($conn, $_REQUEST['data']) : "";
	$sql = "SELECT id FROM userdata WHERE type='refresher' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		$json['success'] = false;
		$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
	}else if (mysqli_num_rows($result)==0){
		$sql = "INSERT INTO userdata (userId, type, data) VALUES ( $userId, 'end', '$data')";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			$json['success'] = false;
			$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
		}else{
			setRefresherComplete($userId);
		}
	}else{
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$sql = "UPDATE userdata SET data='$data' WHERE id=$id";
		$result = mysqli_query($conn, $sql);
		if (!$result){
			$json['success'] = false;
			$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
		}else{
			setRefresherComplete($userId);
		}
	}
	
	echo json_encode($json);
}

function setRefresherComplete($userId){
	global $conn;
	global $json;
	
	$sql = "SELECT id FROM completed WHERE userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		$json['success'] = false;
		$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
		return;
	}
	
	$row = mysqli_fetch_row($result);
	$id = $row[0];
	
	$now = date('Y-m-d H:i:s', time());
	$sql = "UPDATE completed SET refresherCompleted='$now'";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		$json['success'] = false;
		$json['error'] = 'SQL error reading existing data:'.mysqli_error($conn);
	}else{
		$json['success'] = true;
		$json['message'] = 'Refresher complete set';
	}
}

function getStartData($userId){
	global $conn;
	
	$sql = "SELECT data FROM userdata WHERE type='start' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		echo '{ "success":false, "error":"SQL error reading existing start data:'.mysqli_error($conn).'" }';
	}else if (mysqli_num_rows($result)==0){
		echo '{ "success":false, "error":"No start data" }';
	}else{
		$row = mysqli_fetch_assoc($result);
		$data = $row['data'];
		echo '{ "success":true, "data":'.$data.'}';
	}
}

function getEndData($userId){
	global $conn;
	
	$sql = "SELECT data FROM userdata WHERE type='end' AND userId=$userId";
	$result = mysqli_query($conn, $sql);
	
	if (!$result){
		echo '{ "success":false, "error":"SQL error reading existing end data:'.mysqli_error($conn).'" }';
	}else if (mysqli_num_rows($result)==0){
		echo '{ "success":false, "error":"No end data" }';
	}else{
		$row = mysqli_fetch_assoc($result);
		$data = $row['data'];
		echo '{ "success":true, "data":'.$data.'}';
	}
}
?>